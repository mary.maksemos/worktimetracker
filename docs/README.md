WorkTimeTracker


        WorkTimeTracker is a command-line interface (CLI) based Python application designed to help users manage and calculate the total number of hours worked for multiple employers during a given month. The application allows users to add employers, input worked hours, specify hourly rates, calculate pre and post-tax earnings, generate monthly reports, and edit hours and rates. It also integrates with a database for structured data storage.

Installation:
        - Create a virtual environment: <python3 -m venv myenv>
        - Activate the virtual environment: <source myenv/bin/activate> or myenv\Scripts\activate
        - Install required dependencies: <pip install -r requirements.txt>
        - python -m unittest discover -s tests -p test_database_manager.py

Usage:
        - To run the application, execute the following command: <python3 main.py>
        - The application will display a menu with options to perform various tasks.
        - Enter the corresponding option number to select a task and follow the prompts to provide the required information.
        - The application will perform the selected task and display the results or success messages.
         



The functions that are already implemented are:

        - add_employer: Adds a new employer to the database.
        - add_work_hours: Adds work hours for a specific employer to the database.
        - get_all_employers: Retrieves all employers from the database.
        - get_work_hours_by_employer: Retrieves work hours for a specific employer from the database.
        - calculate_monthly_hours: Calculates the total work hours for a specific employer in a given month and year.
        - calculate_monthly_earnings: Calculates the monthly earnings for a specific employer in a given month and year.
        - generate_monthly_report: Generates the monthly report for a specific employer in a given month and year.
        - print_employers: Prints the list of employers.
        - print_work_hours: Prints the list of work hours.
        - main_menu: Displays the main menu and handles user input.


The functions that still need to be implemented are:
       - Delete Employer: Delete an employer from the database.
       - Edit Employer: Edit an employer i the database.
       - Delete Work hours: Delete work hours for a specific employer in a given day, month and year from the database.
       - Edit Work hours: Edit work hours for a specific employer in a given day, month and year from the database.
       - Add more test_function.


Conclusion:

        WorkTimeTracker provides users with a convenient and structured solution for managing and calculating worked hours for multiple employers. By automating calculations, generating reports, and offering editing capabilities, the application simplifies the process of tracking work hours and managing earnings. The integration with a database ensures data organization and accessibility, even when dealing with multiple employers and long time periods.




