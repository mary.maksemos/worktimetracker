# pylint: disable=E0401
"""
WorkTimeTracker
A program for tracking work hours and calculating earnings.
"""
import logging
from database_manager import DatabaseManager

logging.disable(logging.CRITICAL)


def print_employers(employers):
    """
    Print the list of employers.
    """
    print("=" * 55)
    print("Employers:")
    print("-" * 55)
    for employer in employers:
        print(
            f"ID: {employer.id}, Name: {employer.name}, "
            f"Additional Info: {employer.additional_info}"
        )


def print_work_hours(work_hours):
    """
    Print the list of work hours.
    """

    print("=" * 55)
    print(" Work Hours:")
    print("-" * 55)
    for work_hour in work_hours:
        print(
            f"ID: {work_hour.id}, Date: {work_hour.date}, "
            f"Hours: {work_hour.hours}"
        )


def add_employer_menu():
    """
    Menu function to add a new employer.
    """
    name = input("Enter employer name: ")
    additional_info = input("Enter additional information: ")
    hourly_rate = float(input("Enter hourly rate: "))
    db_manager.add_employer(name, additional_info, hourly_rate)
    print("***** Employer added successfully.*****")


def add_work_hours_menu():
    """
    Menu function to add work hours for a specific employer.
    """
    employer_id = int(input("Enter employer ID: "))
    date = input("Enter date (YYYY-MM-DD): ")
    hours = float(input("Enter hours worked: "))
    db_manager.add_work_hours(employer_id, date, hours)
    print("***** Work hours added successfully.*****")


def view_employers_menu():
    """
    Menu function to view all employers.
    """
    employers = db_manager.get_all_employers()
    print_employers(employers)


def view_work_hours_menu():
    """
    Menu function to view work hours for a specific employer.
    """
    employer_id = int(input("Enter employer ID: "))
    work_hours = db_manager.get_work_hours_by_employer(employer_id)
    print_work_hours(work_hours)


def calculate_monthly_hours_menu():
    """
    Menu function to calculate monthly hours for a specific employer.
    """
    employer_id = int(input("Enter employer ID: "))
    month = int(input("Enter month: "))
    year = int(input("Enter year: "))
    total_hours = db_manager.calculate_monthly_hours(employer_id, month, year)
    print("=" * 55)
    print(f"Total hours for the month: {total_hours}")


def calculate_monthly_earnings_menu():
    """
    Menu function to calculate monthly earnings for a specific employer.
    """
    employer_id = int(input("Enter employer ID: "))
    month = int(input("Enter month: "))
    year = int(input("Enter year: "))
    pre_tax_earnings = db_manager.calculate_monthly_earnings(
        employer_id, month, year, pre_tax=True
    )
    post_tax_earnings = db_manager.calculate_monthly_earnings(
        employer_id, month, year, pre_tax=False
    )
    print("=" * 55)
    print("The Monthly Earnings Are:")
    print("-" * 55)
    print(f" Pre-tax earnings for the month: {pre_tax_earnings}")
    print(f" Post-tax earnings for the month: {post_tax_earnings}")


def generate_monthly_report_menu():
    """
    Menu function to generate a monthly report for a specific employer.
    """
    employer_id = int(input("Enter employer ID: "))
    month = int(input("Enter month: "))
    year = int(input("Enter year: "))
    report = db_manager.generate_monthly_report(employer_id, month, year)
    print("=" * 55)
    print("The Report Are:")
    print("-" * 55)
    print(report)


def main_menu():
    """
    Display the main menu and handle user input.
    """
    print("WorkTimeTracker")
    print("-----------------------")
    print("1. Add Employer")
    print("2. Add Work Hours")
    print("3. View Employers")
    print("4. View Work Hours")
    print("5. Calculate Monthly Hours")
    print("6. Calculate Monthly Earnings")
    print("7. Generate Monthly Report")
    print("0. Exit")

    while True:

        choice = input("Select an option: ")

        try:
            if choice == "1":
                add_employer_menu()
            elif choice == "2":
                add_work_hours_menu()
            elif choice == "3":
                view_employers_menu()
            elif choice == "4":
                view_work_hours_menu()
            elif choice == "5":
                calculate_monthly_hours_menu()
            elif choice == "6":
                calculate_monthly_earnings_menu()
            elif choice == "7":
                generate_monthly_report_menu()
            elif choice == "0":
                print("Exiting...")
                break
            else:
                print("Invalid choice. Please try again.")
        except ValueError:
            print("Invalid input. Please enter a valid number.")
        print("/" * 55)
        print("1. Add Employer\t2. Add Work Hours\t3. View Employers")
        print("4. View Work Hours\t5. Calculate Monthly Hours")
        print("6. Calculate Monthly Earnings\t7. Generate Monthly Report")
        print("0. Exit")
        print("-" * 55)


if __name__ == '__main__':
    db_manager = DatabaseManager("sqlite:///worktimetracker.db")
    main_menu()
