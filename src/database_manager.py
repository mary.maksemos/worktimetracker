# pylint: disable=E0401
"""
Module: database_manager
Description: Provides functions to manage
the database for the WorkTimeTracker application.
"""
from datetime import datetime
from calendar import monthrange
from typing import Optional
from sqlmodel import SQLModel, create_engine, Session, select
from database_models import Employer, WorkHours


class DatabaseManager:
    """
    Represents a DatabaseManager for the WorkTimeTracker application.

    This class provides functions to manage the database
    for tracking work hours and calculating earnings.
    """

    def __init__(self, database_url: str):
        self.engine = create_engine(database_url, echo=True)
        self.session = Session(bind=self.engine)
        SQLModel.metadata.create_all(self.engine)

    def add_employer(
        self,
        name: str,
        additional_info: str = "",
        hourly_rate: Optional[float] = None
    ):
        """
        Adds a new employer to the database.

        Args:
            name: The name of the employer.
            additional_info: Additional information
            about the employer (optional).
            hourly_rate: The hourly rate of the employer (optional).
        """
        employer = Employer(
            name=name,
            additional_info=additional_info,
            hourly_rate=hourly_rate
        )
        self.session.add(employer)
        self.session.commit()

    def add_work_hours(
        self,
        employer_id: int,
        date: str,
        hours: float
    ):
        """
        Adds work hours for a specific employer to the database.

        Args:
            employer_id: The ID of the employer.
            date: The date of the work hours in the format 'YYYY-MM-DD'.
            hours: The number of work hours.
        """
        employer = self.session.exec(
            select(Employer).where(Employer.id == employer_id)).first()
        work_hours = WorkHours(employer=employer, date=date, hours=hours)
        self.session.add(work_hours)
        self.session.commit()

    def get_all_employers(self):
        """
        Retrieves all employers from the database.

        Returns:
            A list of all employers.
        """
        employers = self.session.exec(select(Employer)).all()
        return employers

    def get_work_hours_by_employer(self, employer_id: int):
        """
        Retrieves work hours for a specific employer from the database.

        Args:
            employer_id: The ID of the employer.

        Returns:
            A list of work hours for the specified employer.
        """
        work_hours = self.session.exec(
            select(WorkHours)
            .where(WorkHours.employer_id == employer_id)
        ).all()
        return work_hours

    def calculate_monthly_hours(
            self, employer_id: int, month: int, year: int) -> float:
        """
        Calculates the total work hours for
        a specific employer in a given month and year.

        Args:
            employer_id (int): The ID of the employer.
            month (int): The month (1-12).
            year (int): The year.

        Returns:
            float: The total work hours for the specified
            employer in the given month and year.
            If no work hours are found for the specified period,
            returns a string message.
        """
        employer = self.session.query(Employer).get(employer_id)
        if not employer:
            return "Employer not found."

        start_date = datetime(year, month, 1)
        end_date = start_date.replace(day=monthrange(year, month)[1])

        work_hours = (
            self.session.query(WorkHours)
            .filter(
                WorkHours.employer_id == employer_id,
                WorkHours.date.between(start_date, end_date)
            )
            .all()
        )

        if not work_hours:
            return "No work hours found for the specified period."

        total_hours = sum(work.hours for work in work_hours)
        return total_hours

    def calculate_monthly_earnings(
        self,
        employer_id: int,
        month: int,
        year: int,
        pre_tax: bool = True
    ) -> float:
        """
        Calculates the monthly earnings for a specific employer
        in a given month and year.

        Args:
            employer_id (int): The ID of the employer.
            month (int): The month (1-12).
            year (int): The year.
            pre_tax (bool): Whether to calculate earnings
            before tax (default: True).

        Returns:
            float: The monthly earnings for the specified employer
            in the given month and year.
                If the employer is not found or no work hours are found
                for the specified period,
                returns a string message.
        """
        employer = self.session.query(Employer).get(employer_id)
        if not employer:
            return "Employer not found."

        hourly_rate = employer.hourly_rate
        total_hours = self.calculate_monthly_hours(
            employer_id, month, year)
        if isinstance(total_hours, str):
            return total_hours

        earnings = total_hours * hourly_rate
        if not pre_tax:
            tax_rate = 0.3
            earnings *= (1 - tax_rate)

        return earnings

    def generate_monthly_report(
              self, employer_id: int, month: int, year: int) -> str:
        """
        Generates a monthly report for a specific employer,
        displaying their work hours and earnings.

        Args:
            employer_id (int): The ID of the employer.
            month (int): The month (1-12).
            year (int): The year.

        Returns:
            str: The generated monthly report as a string.
                If the employer is not found or no work hours
                are found for the specified period,
                returns a string message.
        """
        employer = self.session.query(Employer).get(employer_id)
        if not employer:
            return "Employer not found."

        start_date = datetime(year, month, 1)
        end_date = start_date.replace(day=monthrange(year, month)[1])

        work_hours = (
            self.session.query(WorkHours)
            .filter(
                WorkHours.employer_id == employer_id,
                WorkHours.date.between(start_date, end_date)
            )
            .all()
        )

        if not work_hours:
            return "No work hours found for the specified period."

        total_hours = sum(work.hours for work in work_hours)
        total_earnings = total_hours * employer.hourly_rate

        report = []
        report.append(f"Employer: {employer.name}")
        report.append(f"Month: {start_date.strftime('%B %Y')}")
        # report.append("Work Hours:")
        for work in work_hours:
            report.append(f"Date: {work.date}, Hours: {work.hours}")
        report.append(f"Total Hours: {total_hours}")
        report.append(f"Pre-tax Earnings: {total_earnings}")

        return "\n".join(report)
